m ^^^ 1 = m
m ^^^ n = m * m ^^^ (n-1)

andy [] = True
andy (b : bs) =  andy bs &&  b

andy2 [] = True
andy2 (b : bs)
  | b = and bs
  | otherwise = False

andy4 [] = False
andy4 (b:bs) = b || and bs

andy3 [] = False
andy3 (b:bs) = andy3 bs && b

andy5 [] = True
andy5 (b:bs) 
  | b == False = False
  | otherwise = and bs

andy6 [] = True
andy6 (b : bs) 
  | b = b
  | otherwise = and bs

(x : _) !!! 0 = x
(_ : xs) !!! n = xs !!! (n-1)

merge [] ys = ys
merge xs [] = xs
merge (x : xs) (y : ys)
  = if x <= y then x : merge xs (y : ys) else y : merge (x : xs) ys

halve xs = splitAt ((length xs) `div` 2)  xs

msort [] = []
msort [x] = [x]
msort xs = merge (msort ys) (msort zs)
  where (ys, zs) = halve xs
