safetail [] = []
safetail (_ : xs) = xs

safetail2 xs
  | null xs = []
  | otherwise = tail xs

{- safetail3 xs = tail xs -}
{- safetail3 [] = [] -}

safetail4
  = \ xs ->
    case xs of
      [] -> []
      (_:xs) -> xs

{- False \\ False = False -}
{- _ \\ _ = True -}

{-
 - T || T = T
 - T || F = T
 - F || T = T
 - F || F = F
 -}

{- False \\ b = b -}
{- True \\ _ = True -}

{- b \\ c -}
  {- | b == c = True -}
  {- | otherwise = False -}

{- False \\ False = False -}
{- False \\ True = True -}
{- True \\ False = True -}
{- True \\ True = True -}

{-
 - T && T = T
 - T && F = F
 - F && T = F
 - F && F = F
 -}

{- a \\ b = if a then if b then True else False else False -}

{- a \\ b = if a then if b then False else  True else False -}
{- a \\ b = if a then b -}

