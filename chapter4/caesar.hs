import Data.Char

let2int :: Char -> Char -> Int
let2int base c = ord c - ord base 

int2let :: Char -> Int -> Char
int2let base n = chr (ord base + n)

shift :: Int -> Char -> Char
shift n c
  | isLower c = int2let 'a' ((let2int 'a' c + n) `mod` 26)
  | isUpper c = int2let 'A' ((let2int 'A' c + n) `mod` 26)
  | otherwise = c

encode :: Int -> String -> String
encode n xs = [shift n x | x <- xs]
