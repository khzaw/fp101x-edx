divides x y = x `mod` y == 0
divisors x = [d | d <- [1..x], x `divides` d]

find k t = [v | (k', v) <- t, k == k']
positions x xs = find x (zip xs [0..n]) where n = length xs - 1
