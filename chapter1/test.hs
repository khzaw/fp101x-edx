double x = x + x

qsort[] = []
qsort(x:xs) =
  (reverse (qsort smaller) ++ [x] ++ reverse (qsort larger))
  where
    smaller = [a | a <- xs, a <= x]
    larger = [b | b <- xs, b > x]

qsort2 [] = []
qsort2 xs = x : qsort larger ++ qsort smaller
  where x = maximum xs
        smaller = [a | a <- xs, a < x]
        larger = [b | b <- xs, b >= x]


n = a `div` length xs
  where a = 10
        xs = [1,2,3,4,5]


last_element xs = reverse xs !! (length xs - 1)
